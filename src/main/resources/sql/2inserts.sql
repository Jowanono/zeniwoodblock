INSERT INTO blocs
VALUES ('FOUR',
        'CIRCLE',
        'GREEN',
        'SATIN',
        'MAHOGANY');

INSERT INTO blocs
VALUES ('SEVEN',
        'HEXAGON',
        'BLUE',
        'BRUT',
        'BEECH');

INSERT INTO blocs
VALUES ('ONE',
        'TRIANGLE',
        'YELLOW',
        'MATT',
        'MAHOGANY');

INSERT INTO blocs
VALUES ('FOUR',
        'RECTANGLE',
        'NONE',
        'SATIN',
        'PINE');
