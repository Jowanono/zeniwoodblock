DROP TABLE if exists blocs;
DROP TABLE if exists users;
DROP TABLE if exists barrels;

CREATE TABLE barrels
(
    id      text PRIMARY KEY,
    userId text REFERENCES users (id)
);

CREATE TABLE blocs
(
    id        text PRIMARY KEY,
    barrelId text REFERENCES barrels (id),
    height    text not null,
    base      text not null,
    measure   text,
    color     text,
    finish    text,
    wood      text
);

CREATE TABLE users
(
    id          text PRIMARY KEY,
    name        text not null,
    surname     text not null,
    mailAdress  text not null,
    adress      text,
    phoneNumber text,
    password    text not null
);

