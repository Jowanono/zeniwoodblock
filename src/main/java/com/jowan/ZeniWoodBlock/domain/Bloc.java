package com.jowan.ZeniWoodBlock.domain;


import com.jowan.ZeniWoodBlock.domain.blocEnum.*;

import javax.persistence.*;

@Entity
@Table(name = "blocs")
public class Bloc {

    @Id
    private String id;
    private Height height;
    private Base base;
    private Color color;
    private Finish finish;
    private Wood wood;
    private Measure measure;

    protected Bloc() {
    }

    public Bloc(String id, Height height, Base base, Measure measure, Color color, Finish finish,
                Wood wood) {
        this.id = id;
        this.height = height;
        this.base = base;
        this.measure = measure;
        this.color = color;
        this.finish = finish;
        this.wood = wood;
    }

    public String getId() {
        return id;
    }

    public Height getHeight() {
        return height;
    }

    public Base getBase() {
        return base;
    }

    public Measure getMeasure() {
        return measure;
    }

    public Color getColor() {
        return color;
    }

    public Finish getFinish() {
        return finish;
    }

    public Wood getWood() {
        return wood;
    }


    public double getBaseArea(Bloc bloc) {
        if (bloc.getBase().equals(Base.SQUARE)) {
            double m = bloc.getMeasure().getValue();
            return m * m;
        } else if (bloc.getBase().equals(Base.TRIANGLE)) {
            return bloc.getMeasure().getValue() / 2;
        } else if (bloc.getBase().equals(Base.RECTANGLE)) {
            return bloc.getMeasure().getValue();
        } else if (bloc.getBase().equals(Base.HEXAGON)) {
            double m = bloc.getMeasure().getValue();
            return (3 * Math.sqrt(3) * m * m) / 2;
        } else {
            double m = bloc.getMeasure().getValue();
            return (3.14 * m * m);
        }
    }

    public double getBasePerimeter(Bloc bloc) {
        if (bloc.getBase().equals(Base.SQUARE)) {
            return 4 * bloc.getMeasure().getValue();
        } else if (bloc.getBase().equals(Base.TRIANGLE)) {
            if (bloc.getMeasure().equals(Measure.SIXBYTEN)) {
                return 28;
            } else if (bloc.getMeasure().equals(Measure.THREEBYFIVE)) {
                return 14;
            } else {
                return 10.5;
            }
        } else if (bloc.getBase().equals(Base.RECTANGLE)) {
            if (bloc.getMeasure().equals(Measure.THREEBYTEN)) {
                return 26;
            } else if (bloc.getMeasure().equals(Measure.ONEBYSEVENPOINTFIVE)) {
                return 17;
            } else {
                return 10;
            }
        } else if (bloc.getBase().equals(Base.HEXAGON)) {
            return 6 * bloc.getMeasure().getValue();
        } else {
            double m = bloc.getMeasure().getValue();
            return (2 * 3.14 * m);
        }
    }

    public double getBlocArea(Bloc bloc) {
        double transitArea = Math.floor((height.getValue() * getBasePerimeter(bloc)) * 100);
        return (transitArea / 100);
    }

    public double getBlocVolume(Bloc bloc) {
        double transitVolume = Math.floor((height.getValue() * getBaseArea(bloc)) * 100);
        return (transitVolume / 100);
    }


    public double getBlocCost(Bloc bloc) {
        if (bloc.getColor().equals(Color.NONE)) {
            double blocVolume = getBlocVolume(bloc);
            double woodCost = bloc.getWood().getValue();
            return (Math.floor(((blocVolume * woodCost) + 0.35) * 100)) / 100;
        } else {
            double blocVolume = getBlocVolume(bloc);
            double woodCost = bloc.getWood().getValue();
            double blocArea = getBlocArea(bloc);
            double paintCost = bloc.getFinish().getValue();
            return (Math.floor((((blocVolume * woodCost) + (blocArea * paintCost) + 0.35)) * 100)) / 100;
        }
    }

    public double getBlocBrutPrice(Bloc bloc) {
        double blocCost = getBlocCost(bloc);
        return (Math.floor((3 * blocCost) * 100)) / 100;
    }
}
