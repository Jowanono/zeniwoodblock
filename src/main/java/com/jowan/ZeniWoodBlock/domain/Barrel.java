package com.jowan.ZeniWoodBlock.domain;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "barrels")
public class Barrel {

    @Id
    @Column(name = "id")
    private String id;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "barrel_id")
    private Set<Bloc> blocList = new HashSet<>();


    protected Barrel() {}

    public Barrel(String id, Set<Bloc> blocList) {
        this.id = id;
        this.blocList = blocList;
    }

    public Set<Bloc> getBlocList() {return blocList;}

    public String getId() { return id; }

    public void setBlocList (Set<Bloc> blocList){this.blocList = blocList;}

    public void setId(String id) { this.id = id; }

    public int getBlocsQuantity(Barrel barrel){
        return barrel.blocList.size();
    }

    public double getBarrelBrutPrice(){
        int blocPriceSum = 0;
        for(Bloc b : this.blocList){
            blocPriceSum += b.getBlocBrutPrice(b);
        }
        return blocPriceSum;
    }

    public double getBarrelNetPrice(){

        return (Math.floor((this.getBarrelBrutPrice() * 1.2) * 100)) / 100;
    }
}
