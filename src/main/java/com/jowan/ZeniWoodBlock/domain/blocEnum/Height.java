package com.jowan.ZeniWoodBlock.domain.blocEnum;

public enum Height {
    ONE(1),
    FOUR(4),
    SEVEN(7);

    private final int value;

    Height(final int newValue){
        value = newValue;
    }

    public int getValue() {return value;}
}
