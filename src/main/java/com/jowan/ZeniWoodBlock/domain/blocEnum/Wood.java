package com.jowan.ZeniWoodBlock.domain.blocEnum;

public enum Wood {
    PINE(0.0009, "pin"),//prix au cm3
    OAK(0.0002, "chêne"),
    BEECH(0.0003, "hêtre"),
    MAHOGANY(0.04, "acajou");

    private final double value;
    private final String name;


    Wood(final double newValue, final String newName) {
        value = newValue;
        name = newName;}

    public double getValue() {return value;}
    public String getName() {return name;}
}
