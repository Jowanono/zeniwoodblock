package com.jowan.ZeniWoodBlock.domain.blocEnum;

public enum Finish {
    MATT(0.00003, "mate"),
    BRIGHT(0.00009, "brillant"),
    SATIN(0.00005, "satiné"),
    BRUT(0.0, "brut");

    private final double value;
    private final String name;

    Finish(final double newValue, final String newName){

        value = newValue;
        name = newName;
    }

    public double getValue() {return value;}
    public String getName() {return name;}
}
