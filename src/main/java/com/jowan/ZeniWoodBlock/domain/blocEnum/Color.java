package com.jowan.ZeniWoodBlock.domain.blocEnum;

public enum Color {
    BLUE("bleu"),
    WHITE("blanc"),
    RED("rouge"),
    YELLOW("jaune"),
    ORANGE("orange"),
    GREEN("vert"),
    BLACK("noir"),
    NONE("naturel");

    private final String value;

    Color(final String newValue){
        value = newValue;
    }

    public String getValue() {return value;}
}
