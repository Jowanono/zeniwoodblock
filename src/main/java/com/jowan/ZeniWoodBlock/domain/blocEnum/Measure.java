package com.jowan.ZeniWoodBlock.domain.blocEnum;

public enum  Measure {
    FIVE(5.0),
    THREE(3.0),
    TWO(2.0),
    ONEPOINTFIVE(1.5),
    ONE(1.0),
    THREEBYTEN(3*10),
    ONEBYSEVENPOINTFIVE(1*7.5),
    ONEBYFOUR(1*4),
    SIXBYTEN(6*10),
    THREEBYFIVE(3*5),
    TWOBYFOUR(2*4);


    private final double value;

    Measure(final double newValue){
        value = newValue;
    }

    public double getValue() {return value;}
}
