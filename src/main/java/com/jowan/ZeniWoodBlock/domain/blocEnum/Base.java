package com.jowan.ZeniWoodBlock.domain.blocEnum;

public enum Base {
    SQUARE("carrée"),
    TRIANGLE("triangulaire"),
    HEXAGON("hexagonale"),
    RECTANGLE("rectangulaire"),
    CIRCLE("circulaire");

    private final String value;

    Base(final String newValue){
        value = newValue;
    }

    public String getValue() {return value;}
}
