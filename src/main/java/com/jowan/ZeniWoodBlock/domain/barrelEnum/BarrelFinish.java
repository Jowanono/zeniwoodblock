package com.jowan.ZeniWoodBlock.domain.barrelEnum;

public enum BarrelFinish {

    MATT,
    BRIGHT,
    SATIN,
    BRUT,
    MIXED
}
