package com.jowan.ZeniWoodBlock.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "users")
public class User {

    @Id
    private String id;
    private String name;
    private String surname;
    @Column(name = "mailadress")
    private String mailAdress;
    private String adress;
    @Column(name = "phonenumber")
    private String phoneNumber;
    private String password;

    protected User(){}

    public User(String id, String name, String surname, String mailAdress, String adress, String phoneNumber,
                String password) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.mailAdress = mailAdress;
        this.adress = adress;
        this.phoneNumber = phoneNumber;
        this.password = password;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getMailAdress() {
        return mailAdress;
    }

    public String getAdress() {
        return adress;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public String getPassword() {
        return password;
    }


}
