package com.jowan.ZeniWoodBlock.services;

import com.jowan.ZeniWoodBlock.controllers.representation.NewUserRepresentation;
import com.jowan.ZeniWoodBlock.domain.User;
import com.jowan.ZeniWoodBlock.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
public class UserService {

    UserRepository userRepository;
    IdGenerator idGenerator;

    @Autowired
    public UserService(UserRepository userRepository, IdGenerator idGenerator) {
        this.userRepository = userRepository;
        this.idGenerator = idGenerator;
    }

    public List<User> getAllUsers() {
        return this.userRepository.findAll();
    }

    public Optional<User> getOneUserById(String id) {
        return this.userRepository.findById(id);
    }

    public User addAUser(NewUserRepresentation u) {
        User user = new User(this.idGenerator.generateNewId(), u.getName(), u.getSurname(), u.getMailAdress(), u.getAdress(),
                u.getPhoneNumber(), u.getPassword());
        this.userRepository.save(user);
        return user;
    }

    public void deleteById(String id) {
        this.userRepository.deleteById(id);
    }

    public void modifiyUser(String id, NewUserRepresentation userRepresentation) {
        User userToChange = this.userRepository.findById(id).get();
        User userChangedByUser = new User(id, userRepresentation.getName(), userRepresentation.getSurname(),
                userRepresentation.getMailAdress(), userRepresentation.getAdress(),
                userRepresentation.getPhoneNumber(), userRepresentation.getPassword());
        String newName;
        String newSurname;
        String newMailAdress;
        String newAdress;
        String newPhoneNumber;
        String newPassword;

        if (userChangedByUser.getName().equals("")) {
            newName = userToChange.getName();
        } else {
            newName = userChangedByUser.getName();
        }

        if (userChangedByUser.getSurname().equals("")) {
            newSurname = userToChange.getSurname();
        } else {
            newSurname = userChangedByUser.getSurname();
        }

        if (userChangedByUser.getMailAdress().equals("")) {
            newMailAdress = userToChange.getMailAdress();
        } else {
            newMailAdress = userChangedByUser.getMailAdress();
        }

        if (userChangedByUser.getAdress().equals("")) {
            newAdress = userToChange.getAdress();
        } else {
            newAdress = userChangedByUser.getAdress();
        }

        if (userChangedByUser.getPhoneNumber().equals("")) {
            newPhoneNumber = userToChange.getPhoneNumber();
        } else {
            newPhoneNumber = userChangedByUser.getPhoneNumber();
        }

        if (userChangedByUser.getPassword().equals("")) {
            newPassword = userToChange.getPassword();
        } else {
            newPassword = userChangedByUser.getPassword();
        }

        User userFinal = new User(id, newName, newSurname, newMailAdress, newAdress,
                newPhoneNumber, newPassword);

        this.userRepository.save(userFinal);

    }
}
