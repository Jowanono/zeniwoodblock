package com.jowan.ZeniWoodBlock.services;

import com.jowan.ZeniWoodBlock.controllers.representation.NewBlocRepresentation;
import com.jowan.ZeniWoodBlock.domain.Bloc;
import com.jowan.ZeniWoodBlock.domain.blocEnum.*;
import com.jowan.ZeniWoodBlock.repositories.BlocRepository;
import com.jowan.ZeniWoodBlock.services.factory.BlocFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.UUID;

@Component
public class InitialDataCreator {

    private BlocRepository blocRepository;
    private BlocFactory blocFactory;
    private BlocService blocService;
    private IdGenerator idGenerator;

    @Autowired
    public InitialDataCreator(BlocRepository blocRepository, BlocFactory blocFactory, BlocService blocService, IdGenerator idGenerator) {

        this.blocRepository = blocRepository;
        this.blocFactory = blocFactory;
        this.blocService = blocService;
        this.idGenerator = idGenerator;
    }

    /*@PostConstruct
    public void createInitialData() {
        this.blocRepository.save(
                new Bloc(UUID.randomUUID().toString(), Height.ONE, Base.SQUARE, Measure.FIVE,
                        Color.BLACK, BarrelFinish.MATT, Wood.PINE));


        this.blocService.addABloc(
                new NewBlocRepresentation(this.idGenerator.generateNewId(), Height.FOUR, Base.CIRCLE,
                        Measure.ONEPOINTFIVE, Color.NONE, BarrelFinish.BRUT, Wood.MAHOGANY));


        this.blocService.addABloc(
                new NewBlocRepresentation(this.idGenerator.generateNewId(), Height.SEVEN, Base.HEXAGON, Measure.ONE,
                        Color.RED, BarrelFinish.BRIGHT, Wood.PINE));

    }*/
}
