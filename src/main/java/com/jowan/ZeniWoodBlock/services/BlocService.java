package com.jowan.ZeniWoodBlock.services;

import com.jowan.ZeniWoodBlock.controllers.representation.NewBlocRepresentation;
import com.jowan.ZeniWoodBlock.domain.Bloc;
import com.jowan.ZeniWoodBlock.domain.blocEnum.*;
import com.jowan.ZeniWoodBlock.repositories.BlocRepository;
import com.jowan.ZeniWoodBlock.services.factory.BlocFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
public class BlocService {

    private BlocRepository blocRepository;
    private IdGenerator idGenerator;
    private BlocFactory blocFactory;

    @Autowired
    public BlocService(BlocRepository blocRepository, IdGenerator idGenerator, BlocFactory blocFactory) {
        this.blocRepository = blocRepository;
        this.idGenerator = idGenerator;
        this.blocFactory = blocFactory;
    }

    public List<Bloc> getAllBlocs() {
        return this.blocRepository.findAll();
    }

    public Optional<Bloc> getOneBlocById(String id) {
        return this.blocRepository.findById(id);
    }

    public void deleteById(String id) {
        this.blocRepository.deleteById(id);
    }

    public Bloc addABloc(NewBlocRepresentation b) {
        Bloc bloc = blocFactory.createBloc(b.getHeight(), b.getBase(), b.getColor(),
                b.getFinish(), b.getWood());
        this.blocRepository.save(bloc);
        return bloc;
    }

    public Bloc addARandomBloc() {
        NewBlocRepresentation randomBlocRepresentation = this.blocFactory.generateRandomBlocRepresentation();
        Bloc bloc = blocFactory.createBloc(randomBlocRepresentation.getHeight(), randomBlocRepresentation.getBase(),
                randomBlocRepresentation.getColor(), randomBlocRepresentation.getFinish(),
                randomBlocRepresentation.getWood());
        this.blocRepository.save(bloc);
        return bloc;
    }



    public void modifiyBloc(String id, NewBlocRepresentation blocRepresentation) {
        Bloc blocToChange = this.blocRepository.findById(id).get();
        Bloc blocChangedByUser = this.blocFactory.createBloc(blocRepresentation.getHeight(),
                blocRepresentation.getBase(), blocRepresentation.getColor(), blocRepresentation.getFinish(),
                blocRepresentation.getWood());
        Height newHeight;
        Base newBase;
        Color newColor;
        Finish newFinish;
        Wood newWood;

        if (blocChangedByUser.getHeight().equals("")) {
            newHeight = blocToChange.getHeight();
        } else {
            newHeight = blocChangedByUser.getHeight();
        }

        if (blocChangedByUser.getBase().equals("")) {
            newBase = blocToChange.getBase();
        } else {
            newBase = blocChangedByUser.getBase();
        }

        if (blocChangedByUser.getColor().equals("")) {
            newColor = blocToChange.getColor();
        } else {
            newColor = blocChangedByUser.getColor();
        }

        if (blocChangedByUser.getFinish().equals("")) {
            newFinish = blocToChange.getFinish();
        } else {
            newFinish = blocChangedByUser.getFinish();
        }

        if (blocChangedByUser.getWood().equals("")) {
            newWood = blocToChange.getWood();
        } else {
            newWood = blocChangedByUser.getWood();
        }

        Bloc transitionBloc = this.blocFactory.createBloc(newHeight, newBase, newColor,
                newFinish, newWood);
        Bloc blocFinal = new Bloc(id, newHeight, newBase,
                transitionBloc.getMeasure(), newColor, newFinish, newWood);

        this.blocRepository.save(blocFinal);

    }

}
