package com.jowan.ZeniWoodBlock.services;

import com.jowan.ZeniWoodBlock.controllers.representation.NewBarrelRepresentation;
import com.jowan.ZeniWoodBlock.controllers.representation.NewBlocRepresentation;
import com.jowan.ZeniWoodBlock.controllers.representation.NewRandomBarrelRepresentation;
import com.jowan.ZeniWoodBlock.domain.Barrel;
import com.jowan.ZeniWoodBlock.domain.Bloc;
import com.jowan.ZeniWoodBlock.domain.barrelEnum.BarrelFinish;
import com.jowan.ZeniWoodBlock.repositories.BarrelRepository;
import com.jowan.ZeniWoodBlock.services.factory.BarrelFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Component
public class BarrelService {

    private BarrelRepository barrelRepository;
    private IdGenerator idGenerator;
    private BlocService blocService;
    private BarrelFactory barrelFactory;

    @Autowired
    public BarrelService(BarrelRepository barrelRepository, IdGenerator idGenerator, BlocService blocService,
                         BarrelFactory barrelFactory) {
        this.barrelRepository = barrelRepository;
        this.idGenerator = idGenerator;
        this.blocService = blocService;
        this.barrelFactory = barrelFactory;
    }

    public List<Barrel> getAllBarrels() {
        return this.barrelRepository.findAll();
    }

    public Optional<Barrel> getOneBarrelById(String id) {
        return this.barrelRepository.findById(id);
    }

    public void deleteById(String id) {
        this.barrelRepository.deleteById(id);
    }

    @Transactional
    public Optional<Barrel> addABarrel(NewBarrelRepresentation b) {
        Set<NewBlocRepresentation> blocRepresentationList = b.getBlocRepresentationList();
        Set<Bloc> blocList = new HashSet<>();
        String idBarrel = this.idGenerator.generateNewId();
        for (NewBlocRepresentation blocRepresentation : blocRepresentationList) {
            Bloc blocToAdd = this.blocService.addABloc(blocRepresentation);
            blocList.add(blocToAdd);
        }
        Barrel barrel = new Barrel(idBarrel, blocList);
        this.barrelRepository.save(barrel);
        return Optional.of(barrel);
    }

    @Transactional
    public Optional<Barrel> addARandomBarrel(NewRandomBarrelRepresentation randomBarrelRepresentation) {
        Barrel barrel = this.barrelFactory.createABarrel(randomBarrelRepresentation.getMaxBarrelPrice(),
                randomBarrelRepresentation.getBarrelFinish());
        this.barrelRepository.save(barrel);
        return Optional.of(barrel);
    }
}
