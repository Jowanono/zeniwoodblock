package com.jowan.ZeniWoodBlock.services.factory;

import com.jowan.ZeniWoodBlock.domain.Bloc;
import com.jowan.ZeniWoodBlock.domain.blocEnum.*;
import com.jowan.ZeniWoodBlock.services.IdGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class BlocWithHexagonBase {

    private IdGenerator idGenerator;

    @Autowired
    public BlocWithHexagonBase(IdGenerator idGenerator) {
        this.idGenerator = idGenerator;
    }

    public Bloc createHexagonBaseBloc(Height height, Color color, Finish finish, Wood wood) {
        switch (height) {
            case ONE:
                return createShortHexagonBloc(color, finish, wood);
            case FOUR:
                return createMediumHexagonBloc(color, finish, wood);
            case SEVEN:
                return createLongHexagonBloc(color, finish, wood);
            default:
                throw new RuntimeException();
        }
    }

    Bloc createShortHexagonBloc(Color color, Finish finish, Wood wood) {
        if(color.equals(Color.NONE)){
            return new Bloc(this.idGenerator.generateNewId(), Height.ONE, Base.HEXAGON, Measure.THREE, Color.NONE,
                    Finish.BRUT, wood );
        } else {
            return new Bloc(this.idGenerator.generateNewId(), Height.ONE, Base.HEXAGON, Measure.THREE, color, finish,
                    Wood.PINE);
        }
    }

    Bloc createMediumHexagonBloc(Color color, Finish finish, Wood wood) {
        if(color.equals(Color.NONE)){
            return new Bloc(this.idGenerator.generateNewId(), Height.FOUR, Base.HEXAGON, Measure.ONEPOINTFIVE,
                    Color.NONE, Finish.BRUT, wood );
        } else {
            return new Bloc(this.idGenerator.generateNewId(), Height.FOUR, Base.HEXAGON, Measure.ONEPOINTFIVE, color,
                    finish, Wood.PINE);
        }
    }

    Bloc createLongHexagonBloc(Color color, Finish finish, Wood wood) {
        if(color.equals(Color.NONE)){
            return new Bloc(this.idGenerator.generateNewId(), Height.SEVEN, Base.HEXAGON, Measure.ONE, Color.NONE,
                    Finish.BRUT, wood );
        } else {
            return new Bloc(this.idGenerator.generateNewId(), Height.SEVEN, Base.HEXAGON, Measure.ONE, color, finish,
                    Wood.PINE);
        }
    }
}
