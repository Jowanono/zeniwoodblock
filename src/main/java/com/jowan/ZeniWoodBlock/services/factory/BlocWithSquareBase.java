package com.jowan.ZeniWoodBlock.services.factory;

import com.jowan.ZeniWoodBlock.domain.Bloc;
import com.jowan.ZeniWoodBlock.domain.blocEnum.*;
import com.jowan.ZeniWoodBlock.services.IdGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class BlocWithSquareBase {

    private IdGenerator idGenerator;

    @Autowired
    public BlocWithSquareBase(IdGenerator idGenerator) {
        this.idGenerator = idGenerator;
    }

    public Bloc createSquareBaseBloc(Height height, Color color, Finish finish, Wood wood) {
        switch (height) {
            case ONE:
                return createShortSquareBloc(color, finish, wood);
            case FOUR:
                return createMediumSquareBloc(color, finish, wood);
            case SEVEN:
                return createLongSquareBloc(color, finish, wood);
            default:
                throw new RuntimeException();
        }
    }

    Bloc createShortSquareBloc(Color color, Finish finish, Wood wood) {
       if(color.equals(Color.NONE)){
           return new Bloc(this.idGenerator.generateNewId(), Height.ONE, Base.SQUARE, Measure.FIVE, Color.NONE,
                   Finish.BRUT, wood );
       } else {
           return new Bloc(this.idGenerator.generateNewId(), Height.ONE, Base.SQUARE, Measure.FIVE, color, finish,
                   Wood.PINE);
       }
    }

    Bloc createMediumSquareBloc(Color color, Finish finish, Wood wood) {
        if(color.equals(Color.NONE)){
            return new Bloc(this.idGenerator.generateNewId(), Height.FOUR, Base.SQUARE, Measure.TWO, Color.NONE,
                    Finish.BRUT, wood );
        } else {
            return new Bloc(this.idGenerator.generateNewId(), Height.FOUR, Base.SQUARE, Measure.TWO, color, finish,
                    Wood.PINE);
        }
    }

    Bloc createLongSquareBloc(Color color, Finish finish, Wood wood) {
        if(color.equals(Color.NONE)){
            return new Bloc(this.idGenerator.generateNewId(), Height.SEVEN, Base.SQUARE, Measure.TWO, Color.NONE,
                    Finish.BRUT, wood );
        } else {
            return new Bloc(this.idGenerator.generateNewId(), Height.SEVEN, Base.SQUARE, Measure.TWO, color, finish,
                    Wood.PINE);
        }
    }

}
