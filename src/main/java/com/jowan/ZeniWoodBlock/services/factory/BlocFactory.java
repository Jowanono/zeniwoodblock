package com.jowan.ZeniWoodBlock.services.factory;

import com.jowan.ZeniWoodBlock.controllers.representation.NewBlocRepresentation;
import com.jowan.ZeniWoodBlock.domain.Bloc;
import com.jowan.ZeniWoodBlock.domain.blocEnum.*;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Random;

@Component
public class BlocFactory {

    BlocWithSquareBase blocWithSquareBase;
    BlocWithTriangleBase blocWithTriangleBase;
    BlocWithRectangleBase blocWithRectangleBase;
    BlocWithHexagonBase blocWithHexagonBase;
    BlocWithCircleBase blocWithCircleBase;

    public BlocFactory(BlocWithSquareBase blocWithSquareBase, BlocWithTriangleBase blocWithTriangleBase,
                       BlocWithRectangleBase blocWithRectangleBase, BlocWithHexagonBase blocWithHexagonBase,
                       BlocWithCircleBase blocWithCircleBase) {
        this.blocWithSquareBase = blocWithSquareBase;
        this.blocWithTriangleBase = blocWithTriangleBase;
        this.blocWithRectangleBase = blocWithRectangleBase;
        this.blocWithHexagonBase = blocWithHexagonBase;
        this.blocWithCircleBase = blocWithCircleBase;
    }

    public Bloc createBloc(Height height, Base base, Color color, Finish finish, Wood wood) {
        switch (base) {
            case SQUARE:
                return blocWithSquareBase.createSquareBaseBloc(height, color, finish, wood);
            case TRIANGLE:
                return blocWithTriangleBase.createTriangleBaseBloc(height, color, finish, wood);
            case RECTANGLE:
                return blocWithRectangleBase.createRectangleBaseBloc(height, color, finish, wood);
            case HEXAGON:
                return blocWithHexagonBase.createHexagonBaseBloc(height, color, finish, wood);
            case CIRCLE:
                return blocWithCircleBase.createCircleBaseBloc(height, color, finish, wood);
            default:
                throw new RuntimeException();
        }
    }

    public NewBlocRepresentation generateRandomBlocRepresentation(){
        Height[] heights = Height.values();
        Base[] bases = Base.values();
        Color[] colors = Color.values();
        Finish[] finishes = Finish.values();
        Wood[] woods = Wood.values();
        Random rnd = new Random();

        return new NewBlocRepresentation(
                heights[rnd.nextInt(heights.length)],
                bases[rnd.nextInt(bases.length)],
                colors[rnd.nextInt(colors.length)],
                finishes[rnd.nextInt(finishes.length)],
                woods[rnd.nextInt(woods.length)]
        );
    }
}
