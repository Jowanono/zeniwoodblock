package com.jowan.ZeniWoodBlock.services.factory;

import com.jowan.ZeniWoodBlock.domain.Bloc;
import com.jowan.ZeniWoodBlock.domain.blocEnum.*;
import com.jowan.ZeniWoodBlock.services.IdGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class BlocWithRectangleBase {

    private IdGenerator idGenerator;

    @Autowired
    public BlocWithRectangleBase(IdGenerator idGenerator) {
        this.idGenerator = idGenerator;
    }

    public Bloc createRectangleBaseBloc(Height height, Color color, Finish finish, Wood wood) {
        switch (height) {
            case ONE:
                return createShortRectangleBloc(color, finish, wood);
            case FOUR:
                return createMediumRectangleBloc(color, finish, wood);
            case SEVEN:
                return createLongRectangleBloc(color, finish, wood);
            default:
                throw new RuntimeException();
        }
    }

    Bloc createShortRectangleBloc(Color color, Finish finish, Wood wood) {
        if(color.equals(Color.NONE)){
            return new Bloc(this.idGenerator.generateNewId(), Height.ONE, Base.RECTANGLE, Measure.THREEBYTEN, Color.NONE,
                    Finish.BRUT, wood );
        } else {
            return new Bloc(this.idGenerator.generateNewId(), Height.ONE, Base.RECTANGLE, Measure.THREEBYTEN,
                    color, finish, Wood.PINE);
        }
    }

    Bloc createMediumRectangleBloc(Color color, Finish finish, Wood wood) {
        if(color.equals(Color.NONE)){
            return new Bloc(this.idGenerator.generateNewId(), Height.FOUR, Base.RECTANGLE, Measure.ONEBYSEVENPOINTFIVE,
                    Color.NONE, Finish.BRUT, wood );
        } else {
            return new Bloc(this.idGenerator.generateNewId(), Height.FOUR, Base.RECTANGLE, Measure.ONEBYSEVENPOINTFIVE,
                    color, finish, Wood.PINE);
        }
    }

    Bloc createLongRectangleBloc(Color color, Finish finish, Wood wood) {
        if(color.equals(Color.NONE)){
            return new Bloc(this.idGenerator.generateNewId(), Height.SEVEN, Base.RECTANGLE, Measure.ONEBYFOUR,
                    Color.NONE, Finish.BRUT, wood );
        } else {
            return new Bloc(this.idGenerator.generateNewId(), Height.SEVEN, Base.RECTANGLE, Measure.ONEBYFOUR, color,
                    finish, Wood.PINE);
        }
    }
}
