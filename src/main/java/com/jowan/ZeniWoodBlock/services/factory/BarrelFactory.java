package com.jowan.ZeniWoodBlock.services.factory;

import com.jowan.ZeniWoodBlock.controllers.representation.NewBlocRepresentation;
import com.jowan.ZeniWoodBlock.domain.Barrel;
import com.jowan.ZeniWoodBlock.domain.Bloc;
import com.jowan.ZeniWoodBlock.domain.barrelEnum.BarrelFinish;
import com.jowan.ZeniWoodBlock.domain.blocEnum.Color;
import com.jowan.ZeniWoodBlock.domain.blocEnum.Finish;
import com.jowan.ZeniWoodBlock.domain.blocEnum.Wood;
import com.jowan.ZeniWoodBlock.repositories.BlocRepository;
import com.jowan.ZeniWoodBlock.services.BlocService;
import com.jowan.ZeniWoodBlock.services.IdGenerator;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Set;

@Component
public class BarrelFactory {

    IdGenerator idGenerator;
    BlocFactory blocFactory;
    BlocService blocService;
    BlocRepository blocRepository;

    public BarrelFactory(IdGenerator idGenerator, BlocFactory blocFactory, BlocService blocService,
                         BlocRepository blocRepository) {

        this.idGenerator = idGenerator;
        this.blocFactory = blocFactory;
        this.blocService = blocService;
        this.blocRepository = blocRepository;
    }

    public Barrel createABarrel(double maxBarrelPrice, BarrelFinish barrelFinish) {
        Set<Bloc> blocList = new HashSet<>();
        Barrel barrel = new Barrel(this.idGenerator.generateNewId(), blocList);
        double barrelNetPrice = 0;
        switch (barrelFinish) {
            case BRUT: {
                while (barrelNetPrice < maxBarrelPrice) {
                    NewBlocRepresentation randomBlocRepresentation = this.blocFactory.generateRandomBlocRepresentation();
                    Bloc bloc = blocFactory.createBloc(randomBlocRepresentation.getHeight(),
                            randomBlocRepresentation.getBase(), Color.NONE, Finish.BRUT,
                            randomBlocRepresentation.getWood());
                    this.blocRepository.save(bloc);
                    blocList.add(bloc);
                    barrelNetPrice = barrel.getBarrelNetPrice();
                }

            }

            case MATT: {
                while (barrelNetPrice < maxBarrelPrice) {
                    NewBlocRepresentation randomBlocRepresentation = this.blocFactory.generateRandomBlocRepresentation();
                    Bloc bloc = blocFactory.createBloc(randomBlocRepresentation.getHeight(),
                            randomBlocRepresentation.getBase(), randomBlocRepresentation.getColor(), Finish.MATT,
                            Wood.PINE);
                    this.blocRepository.save(bloc);
                    blocList.add(bloc);
                    barrelNetPrice = barrel.getBarrelNetPrice();
                }

            }

            case BRIGHT: {
                while (barrelNetPrice < maxBarrelPrice) {
                    NewBlocRepresentation randomBlocRepresentation = this.blocFactory.generateRandomBlocRepresentation();
                    Bloc bloc = blocFactory.createBloc(randomBlocRepresentation.getHeight(),
                            randomBlocRepresentation.getBase(), randomBlocRepresentation.getColor(), Finish.BRIGHT,
                            Wood.PINE);
                    this.blocRepository.save(bloc);
                    blocList.add(bloc);
                    barrelNetPrice = barrel.getBarrelNetPrice();
                }

            }

            case SATIN: {
                while (barrelNetPrice < maxBarrelPrice) {
                    NewBlocRepresentation randomBlocRepresentation = this.blocFactory.generateRandomBlocRepresentation();
                    Bloc bloc = blocFactory.createBloc(randomBlocRepresentation.getHeight(),
                            randomBlocRepresentation.getBase(), randomBlocRepresentation.getColor(), Finish.SATIN,
                            Wood.PINE);
                    this.blocRepository.save(bloc);
                    blocList.add(bloc);
                    barrelNetPrice = barrel.getBarrelNetPrice();
                }

            }

            case MIXED: {
                while (barrelNetPrice < maxBarrelPrice) {
                    NewBlocRepresentation randomBlocRepresentation = this.blocFactory.generateRandomBlocRepresentation();
                    Bloc bloc = blocFactory.createBloc(randomBlocRepresentation.getHeight(), randomBlocRepresentation.getBase(),
                            randomBlocRepresentation.getColor(), randomBlocRepresentation.getFinish(),
                            randomBlocRepresentation.getWood());
                    this.blocRepository.save(bloc);
                    blocList.add(bloc);
                    barrelNetPrice = barrel.getBarrelNetPrice();
                }

            }
        }

        return barrel;


    }
}
