package com.jowan.ZeniWoodBlock.services.factory;

import com.jowan.ZeniWoodBlock.domain.Bloc;
import com.jowan.ZeniWoodBlock.domain.blocEnum.*;
import com.jowan.ZeniWoodBlock.services.IdGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class BlocWithTriangleBase {

    private IdGenerator idGenerator;

    @Autowired
    public BlocWithTriangleBase(IdGenerator idGenerator) {
        this.idGenerator = idGenerator;
    }

    public Bloc createTriangleBaseBloc(Height height, Color color, Finish finish, Wood wood) {
        switch (height) {
            case ONE:
                return createShortTriangleBloc(color, finish, wood);
            case FOUR:
                return createMediumTriangleBloc(color, finish, wood);
            case SEVEN:
                return createLongTriangleBloc(color, finish, wood);
            default:
                throw new RuntimeException();
        }
    }

    Bloc createShortTriangleBloc(Color color, Finish finish, Wood wood) {
        if(color.equals(Color.NONE)){
            return new Bloc(this.idGenerator.generateNewId(), Height.ONE, Base.TRIANGLE, Measure.SIXBYTEN, Color.NONE,
                    Finish.BRUT, wood );
        } else {
            return new Bloc(this.idGenerator.generateNewId(), Height.ONE, Base.TRIANGLE, Measure.SIXBYTEN, color,
                    finish, Wood.PINE);
        }
    }

    Bloc createMediumTriangleBloc(Color color, Finish finish, Wood wood) {
        if(color.equals(Color.NONE)){
            return new Bloc(this.idGenerator.generateNewId(), Height.FOUR, Base.TRIANGLE, Measure.THREEBYFIVE,
                    Color.NONE, Finish.BRUT, wood );
        } else {
            return new Bloc(this.idGenerator.generateNewId(), Height.FOUR, Base.TRIANGLE, Measure.THREEBYFIVE,
                    color, finish, Wood.PINE);
        }
    }

    Bloc createLongTriangleBloc(Color color, Finish finish, Wood wood) {
        if(color.equals(Color.NONE)){
            return new Bloc(this.idGenerator.generateNewId(), Height.SEVEN, Base.TRIANGLE, Measure.TWOBYFOUR, Color.NONE,
                    Finish.BRUT, wood );
        } else {
            return new Bloc(this.idGenerator.generateNewId(), Height.SEVEN, Base.TRIANGLE, Measure.TWOBYFOUR, color,
                    finish, Wood.PINE);
        }
    }
}
