package com.jowan.ZeniWoodBlock.services.factory;

import com.jowan.ZeniWoodBlock.domain.Bloc;
import com.jowan.ZeniWoodBlock.domain.blocEnum.*;
import com.jowan.ZeniWoodBlock.services.IdGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class BlocWithCircleBase {

    private IdGenerator idGenerator;

    @Autowired
    public BlocWithCircleBase(IdGenerator idGenerator) {
        this.idGenerator = idGenerator;
    }

    public Bloc createCircleBaseBloc(Height height, Color color, Finish finish, Wood wood) {
        switch (height) {
            case ONE:
                return createShortCircleBloc(color, finish, wood);
            case FOUR:
                return createMediumCircleBloc(color, finish, wood);
            case SEVEN:
                return createLongCircleBloc(color, finish, wood);
            default:
                throw new RuntimeException();
        }
    }

    Bloc createShortCircleBloc(Color color, Finish finish, Wood wood) {
        if (color.equals(Color.NONE)) {
            return new Bloc(this.idGenerator.generateNewId(), Height.ONE, Base.CIRCLE, Measure.THREE, Color.NONE,
                    Finish.BRUT, wood);
        } else {
            return new Bloc(this.idGenerator.generateNewId(), Height.ONE, Base.CIRCLE, Measure.THREE, color, finish,
                    Wood.PINE);
        }
    }

    Bloc createMediumCircleBloc(Color color, Finish finish, Wood wood) {
        if (color.equals(Color.NONE)) {
            return new Bloc(this.idGenerator.generateNewId(), Height.FOUR, Base.CIRCLE, Measure.ONEPOINTFIVE, Color.NONE,
                    Finish.BRUT, wood);
        } else {
            return new Bloc(this.idGenerator.generateNewId(), Height.FOUR, Base.CIRCLE, Measure.ONEPOINTFIVE, color,
                    finish, Wood.PINE);
        }
    }

    Bloc createLongCircleBloc(Color color, Finish finish, Wood wood) {
        if (color.equals(Color.NONE)) {
            return new Bloc(this.idGenerator.generateNewId(), Height.SEVEN, Base.CIRCLE, Measure.ONE, Color.NONE,
                    Finish.BRUT, wood);
        } else {
            return new Bloc(this.idGenerator.generateNewId(), Height.SEVEN, Base.CIRCLE, Measure.ONE, color, finish,
                    Wood.PINE);
        }
    }
}
