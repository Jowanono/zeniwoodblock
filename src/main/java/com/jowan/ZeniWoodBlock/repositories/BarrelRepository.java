package com.jowan.ZeniWoodBlock.repositories;

import com.jowan.ZeniWoodBlock.domain.Barrel;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface BarrelRepository extends CrudRepository<Barrel, String> {

    List<Barrel> findAll();
}
