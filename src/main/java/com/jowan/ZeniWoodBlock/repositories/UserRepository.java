package com.jowan.ZeniWoodBlock.repositories;

import com.jowan.ZeniWoodBlock.domain.User;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface UserRepository extends CrudRepository<User, String> {

    List<User> findAll();

}
