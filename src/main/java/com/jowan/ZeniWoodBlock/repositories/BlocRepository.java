package com.jowan.ZeniWoodBlock.repositories;

import com.jowan.ZeniWoodBlock.domain.Bloc;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface BlocRepository extends CrudRepository<Bloc, String> {

    List<Bloc> findAll();

}
