package com.jowan.ZeniWoodBlock;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ZeniWoodBlockApplication {

	public static void main(String[] args) {
		SpringApplication.run(ZeniWoodBlockApplication.class, args);
	}

}
