package com.jowan.ZeniWoodBlock.controllers;

import com.jowan.ZeniWoodBlock.controllers.representation.BarrelRepresentationMapper;
import com.jowan.ZeniWoodBlock.controllers.representation.DisplayableBarrelRepresentation;
import com.jowan.ZeniWoodBlock.controllers.representation.NewBarrelRepresentation;
import com.jowan.ZeniWoodBlock.controllers.representation.NewRandomBarrelRepresentation;
import com.jowan.ZeniWoodBlock.domain.Barrel;
import com.jowan.ZeniWoodBlock.services.BarrelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/ZeniWoodBlock")
public class BarrelController {

    private BarrelService barrelService;
    private BarrelRepresentationMapper mapper;

    @Autowired
    public BarrelController(BarrelService barrelService, BarrelRepresentationMapper mapper) {
        this.barrelService = barrelService;
        this.mapper = mapper;
    }

    @GetMapping("/barrels")
    List<DisplayableBarrelRepresentation> getAllBarrels() {
        return this.barrelService.getAllBarrels().stream()
                .map(this.mapper::mapToDisplayableBarrel)
                .collect(Collectors.toList());
    }

    @GetMapping("/barrels/{id}")
    ResponseEntity<DisplayableBarrelRepresentation> getOneBarrelById(@PathVariable("id") String id) {
        Optional<Barrel> optionalBarrel = this.barrelService.getOneBarrelById(id);

        return optionalBarrel
                .map(this.mapper::mapToDisplayableBarrel)
                .map(ResponseEntity::ok)
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    @DeleteMapping("/barrels/{id}")
    void deleteBarrelById(@PathVariable("id") String id) {
        this.barrelService.deleteById(id);

    }

    @PostMapping("/barrels")
    Optional<Barrel> createABarrel(@RequestBody NewBarrelRepresentation barrelrepresentation) {
        return this.barrelService.addABarrel(barrelrepresentation);

    }

    @PostMapping("/randomBarrels")
    Optional<Barrel> createARandomBarrel (@RequestBody NewRandomBarrelRepresentation randomBarrelRepresentation) {
        return this.barrelService.addARandomBarrel(randomBarrelRepresentation);
    }
}
