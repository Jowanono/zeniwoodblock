package com.jowan.ZeniWoodBlock.controllers;

import com.jowan.ZeniWoodBlock.controllers.representation.DisplayableUserRepresentation;
import com.jowan.ZeniWoodBlock.controllers.representation.NewUserRepresentation;
import com.jowan.ZeniWoodBlock.controllers.representation.UserRepresentationMapper;
import com.jowan.ZeniWoodBlock.domain.User;
import com.jowan.ZeniWoodBlock.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/ZeniWoodBlock")
public class UserController {

    private UserService userService;
    private UserRepresentationMapper mapper;

    @Autowired
    public UserController(UserService blocService, UserRepresentationMapper mapper) {
        this.userService = blocService;
        this.mapper = mapper;
    }

    @GetMapping("/users")
    List<DisplayableUserRepresentation> getAllUsers() {
        return this.userService.getAllUsers().stream()
                .map(this.mapper::mapToDisplayableUser)
                .collect(Collectors.toList());
    }

    @GetMapping("/users/{id}")
    ResponseEntity<DisplayableUserRepresentation> getOneUserById(@PathVariable("id") String id) {
        Optional<User> optionalUser = this.userService.getOneUserById(id);

        return optionalUser
                .map(this.mapper::mapToDisplayableUser)
                .map(ResponseEntity::ok)
                .orElseGet(()-> ResponseEntity.notFound().build());
    }

    @DeleteMapping("/users/{id}")
    void deleteUserById (@PathVariable("id") String id) {
        this.userService.deleteById(id);

    }

    @PostMapping("/users")
    User createUser(@RequestBody NewUserRepresentation userRepresentation) {
        return this.userService.addAUser(userRepresentation);

    }

    @PutMapping("/users/{id}")
    ResponseEntity<DisplayableUserRepresentation> modifyUser (@PathVariable("id") String id,
                                                              @RequestBody NewUserRepresentation userRepresentation) {
        this.userService.modifiyUser(id, userRepresentation);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }
}
