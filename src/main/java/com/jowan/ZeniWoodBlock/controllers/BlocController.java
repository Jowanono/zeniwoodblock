package com.jowan.ZeniWoodBlock.controllers;

import com.jowan.ZeniWoodBlock.controllers.representation.BlocRepresentationMapper;
import com.jowan.ZeniWoodBlock.controllers.representation.DisplayableBlocRepresentation;
import com.jowan.ZeniWoodBlock.controllers.representation.NewBlocRepresentation;
import com.jowan.ZeniWoodBlock.domain.Bloc;
import com.jowan.ZeniWoodBlock.services.BlocService;
import com.jowan.ZeniWoodBlock.services.IdGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/ZeniWoodBlock")
public class BlocController {

    private BlocService blocService;
    private BlocRepresentationMapper mapper;
    private IdGenerator idGenerator;

    @Autowired
    public BlocController(BlocService blocService, BlocRepresentationMapper mapper, IdGenerator idGenerator) {
        this.blocService = blocService;
        this.mapper = mapper;
        this.idGenerator = idGenerator;
    }

    @GetMapping("/blocs")
    List<DisplayableBlocRepresentation> getAllBlocsId() {
        return this.blocService.getAllBlocs().stream()
                .map(this.mapper::mapToDisplayableBloc)
                .collect(Collectors.toList());
    }

    @GetMapping("/blocs/{id}")
    ResponseEntity<DisplayableBlocRepresentation> getOneBlocById(@PathVariable("id") String id) {
        Optional<com.jowan.ZeniWoodBlock.domain.Bloc> optionalBloc = this.blocService.getOneBlocById(id);

        return optionalBloc
                .map(this.mapper::mapToDisplayableBloc)
                .map(ResponseEntity::ok)
                .orElseGet(()-> ResponseEntity.notFound().build());
    }

    @DeleteMapping("/blocs/{id}")
    void deleteBlocById (@PathVariable("id") String id) {
        this.blocService.deleteById(id);

    }

    @PostMapping("/blocs")
    Bloc createABloc(@RequestBody NewBlocRepresentation blocrepresentation) {
        return this.blocService.addABloc(blocrepresentation);

    }

    @PostMapping("/randomBlocs")
    Bloc createARandomBloc() {
        return this.blocService.addARandomBloc();

    }

    @PutMapping("/blocs/{id}")
    ResponseEntity<DisplayableBlocRepresentation> modifyBloc (@PathVariable("id") String id,
                                                              @RequestBody NewBlocRepresentation blocrepresentation) {
        this.blocService.modifiyBloc(id, blocrepresentation);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

}
