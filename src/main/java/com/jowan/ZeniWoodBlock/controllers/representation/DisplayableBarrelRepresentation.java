package com.jowan.ZeniWoodBlock.controllers.representation;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class DisplayableBarrelRepresentation {

    private String BlocsQuantity;
    private String BrutPrice;
    private String NetPrice;

    public String getBlocsQuantity() {
        return BlocsQuantity;
    }

    public String getBrutPrice() {
        return BrutPrice;
    }

    public String getNetPrice() { return NetPrice; }

    public void setBlocsQuantity(String blocsQuantity) {
        BlocsQuantity = blocsQuantity;
    }

    public void setBrutPrice(String brutPrice) { BrutPrice = brutPrice; }

    public void setNetPrice(String netPrice) { NetPrice = netPrice; }
}
