package com.jowan.ZeniWoodBlock.controllers.representation;

import java.util.Set;

public class NewBarrelRepresentation {

    private Set<NewBlocRepresentation> blocRepresentationList;

    public NewBarrelRepresentation(Set<NewBlocRepresentation> blocRepresentationList) {

        this.blocRepresentationList = blocRepresentationList;
    }

    public Set<NewBlocRepresentation> getBlocRepresentationList() {
        return blocRepresentationList;
    }

    public void setBlocRepresentationList(Set<NewBlocRepresentation> blocRepresentationList) {
        this.blocRepresentationList = blocRepresentationList;
    }




}
