package com.jowan.ZeniWoodBlock.controllers.representation;

public class NewUserRepresentation {

    private String name;
    private String surname;
    private String mailAdress;
    private String adress;
    private String phoneNumber;
    private String password;

    public NewUserRepresentation(String name, String surname, String mailAdress, String adress,
                                 String phoneNumber, String password) {

        this.name = name;
        this.surname = surname;
        this.mailAdress = mailAdress;
        this.adress = adress;
        this.phoneNumber = phoneNumber;
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getMailAdress() {
        return mailAdress;
    }

    public String getAdress() {
        return adress;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public String getPassword() {
        return password;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setMailAdress(String mailAdress) {
        this.mailAdress = mailAdress;
    }

    public void setAdress(String adress) {
        this.adress = adress;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
