package com.jowan.ZeniWoodBlock.controllers.representation;

import com.jowan.ZeniWoodBlock.domain.Bloc;
import org.springframework.stereotype.Component;

@Component
public class BlocRepresentationMapper {

    public DisplayableBlocRepresentation mapToDisplayableBloc(Bloc b) {
        DisplayableBlocRepresentation result = new DisplayableBlocRepresentation();
        result.setName("Bloc de base " +b.getBase().getValue()+ ", de hauteur " +b.getHeight().getValue()+ " cm, en "
                +b.getWood().getName()+ " " +b.getColor().getValue()+ " " +b.getFinish().getName());
        result.setCost(b.getBlocCost(b)+ " euros");
        result.setBrutPrice(b.getBlocBrutPrice(b)+ " euros");

        return result;
    }
}
