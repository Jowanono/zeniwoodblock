package com.jowan.ZeniWoodBlock.controllers.representation;

import com.jowan.ZeniWoodBlock.domain.Barrel;
import org.springframework.stereotype.Component;

@Component
public class BarrelRepresentationMapper {

    public DisplayableBarrelRepresentation mapToDisplayableBarrel(Barrel b) {
        DisplayableBarrelRepresentation result = new DisplayableBarrelRepresentation();
        result.setBlocsQuantity(b.getBlocsQuantity(b)+ " blocs");
        result.setBrutPrice(b.getBarrelBrutPrice()+ " euros");
        result.setNetPrice(b.getBarrelNetPrice()+ " euros");

        return result;
    }
}
