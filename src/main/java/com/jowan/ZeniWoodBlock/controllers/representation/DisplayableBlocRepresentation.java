package com.jowan.ZeniWoodBlock.controllers.representation;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class DisplayableBlocRepresentation {

    private String name;
    private String cost;
    private String brutPrice;

    public String getName() {
        return name;
    }

    public String getCost() {
        return cost;
    }

    public String getBrutPrice() { return brutPrice; }

    public void setName(String name) { this.name = name; }

    public void setCost(String cost) {
        this.cost = cost;
    }

    public void setBrutPrice(String brutPrice) { this.brutPrice = brutPrice; }
}
