package com.jowan.ZeniWoodBlock.controllers.representation;

import com.jowan.ZeniWoodBlock.domain.blocEnum.*;

public class NewBlocRepresentation {

    private Height height;
    private Base base;
    private Color color;
    private Finish finish;
    private Wood wood;

    public NewBlocRepresentation(Height height, Base base, Color color, Finish finish, Wood wood) {

        this.height = height;
        this.base = base;
        this.color = color;
        this.finish = finish;
        this.wood = wood;
    }

    public Height getHeight() {
        return height;
    }

    public Base getBase() {
        return base;
    }

    public Color getColor() {
        return color;
    }

    public Finish getFinish() {
        return finish;
    }

    public Wood getWood() {
        return wood;
    }

    public void setHeight(Height height) {
        this.height = height;
    }

    public void setBase(Base base) {
        this.base = base;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public void setFinish(Finish finish) {
        this.finish = finish;
    }

    public void setWood(Wood wood) {
        this.wood = wood;
    }

}
