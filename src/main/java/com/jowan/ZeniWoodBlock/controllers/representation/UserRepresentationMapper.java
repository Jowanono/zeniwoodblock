package com.jowan.ZeniWoodBlock.controllers.representation;

import com.jowan.ZeniWoodBlock.domain.User;
import org.springframework.stereotype.Component;

@Component
public class UserRepresentationMapper {

    public DisplayableUserRepresentation mapToDisplayableUser(User u) {
        DisplayableUserRepresentation result = new DisplayableUserRepresentation();
        result.setId(u.getId());
        result.setName(u.getName());
        result.setSurname(u.getSurname());

        return result;
    }
}
