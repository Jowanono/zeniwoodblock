package com.jowan.ZeniWoodBlock.controllers.representation;

import com.jowan.ZeniWoodBlock.domain.barrelEnum.BarrelFinish;

public class NewRandomBarrelRepresentation {

    double maxBarrelPrice;
    BarrelFinish barrelFinish;

    public NewRandomBarrelRepresentation(double maxBarrelPrice, BarrelFinish barrelFinish) {
        this.maxBarrelPrice = maxBarrelPrice;
        this.barrelFinish = barrelFinish;
    }

    public double getMaxBarrelPrice() { return maxBarrelPrice; }

    public BarrelFinish getBarrelFinish() { return barrelFinish; }

    public void setMaxBarrelPrice(double maxBarrelPrice) { this.maxBarrelPrice = maxBarrelPrice; }

    public void setBarrelFinish(BarrelFinish barrelFinish) { this.barrelFinish = barrelFinish; }
}
